/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of "Maths Code", distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

use std::{io, process};

fn main() {
	println!("Enter the greater number (a): ");
	let input_a = read_num();
	println!("Enter the smaller number (b): ");
	let input_b = read_num();

	let gcd = find_gcd(input_a, input_b);

	println!("{} and {} have the greatest common divisor {}", input_a, input_b, gcd);
}

fn find_gcd(a: u128, b: u128) -> u128 {
	// If b = 0, we've found our answer
	if b == 0 {
		return a;
	}

	let c = a % b;

	find_gcd(b, c)
}

fn read_num() -> u128 {
	let mut input_string = String::new();

	io::stdin()
		.read_line(&mut input_string)
		.expect("Failed to read from stdin");

	let input_string = input_string.trim();

	input_string.parse::<u128>()
		.unwrap_or_else(|_| {
			println!("Your number was either to big, not a number, or not a decimal number");
			process::exit(0);
		})
}
