/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of "Maths Code", distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

use std::{io, process};

fn main() {
    println!("Enter a number: ");

	let mut input_text = String::new();
	io::stdin()
		.read_line(&mut input_text)
		.expect("Failed to read from stdin");

	let text_trimmed = input_text.trim();

	let integer = text_trimmed.parse::<u128>()
		.unwrap_or_else(|_| {
			println!("Your number was either too big, not a number, or not a decimal number");
			process::exit(0);
		});

	match is_abundant(integer) {
		Some(abundance_info) => {
			println!("Your number is abundant.\nAbundance: {}\nProper divisors: {:?}",
				abundance_info.0, abundance_info.1);
		},
		None => println!("Your number is not abundant.")
	}
}

fn is_abundant(number: u128) -> Option<(u128, Vec<u128>)> {
	// 0 is not abundant, but code that follows crashes with 0 as input
	if number == 0 {
		return None
	}

	let mut accumulator: u128 = 0;
	let mut divisors = Vec::new();

	for i in 1..(number - 1) {
		if number % i == 0 {
			divisors.push(i);
			accumulator += i;
		}
	}

	if accumulator > number {
		Some((accumulator - number, divisors))
	} else {
		None
	}
}
