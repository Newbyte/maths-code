use std::{clone, cmp, vec};

fn main() {
    let to_sort = vec![2, 0, 9, 3, 6, 29, 31, 2];
    let sorted = quicksort(to_sort);
    assert_eq!(sorted, [0, 2, 2, 3, 6, 9, 29, 31]);

    let to_sort = vec![0.2, 1.0, 9.3, 0.111, 10.0, 0.0];
    let sorted = quicksort(to_sort);
    assert_eq!(sorted, [0.0, 0.111, 0.2, 1.0, 9.3, 10.0]);
}

fn quicksort<T: clone::Clone + cmp::PartialOrd>(mut to_sort: vec::Vec<T>) -> vec::Vec<T> {
	let pivot = match to_sort.pop() {
		Some(pivot) => pivot,
		None => return Vec::new(),
	};

	let mut greater_elements = Vec::new();
	let mut smaller_elements = Vec::new();

	for element in to_sort {
		if element > pivot {
			greater_elements.push(element);
		} else {
			smaller_elements.push(element);
		}
	}

	let greater_sorted = quicksort(greater_elements);
	let smaller_sorted = quicksort(smaller_elements);

	[smaller_sorted, vec![pivot], greater_sorted].concat()
}
